package dev.cancio.finaldex.data.model

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Check
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material.icons.outlined.Star
import androidx.compose.ui.graphics.vector.ImageVector
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Pokemon(
    @SerializedName("id")
    val pokemonId: Int? = null,
    val url: String = "https://pokeapi.co/api/v2/pokemon/0/",
    val name: String = "who that's pokemon",
    var like: LikeType = LikeType.Unlike
):Serializable {
    val id: Int
        get() = pokemonId ?: url.removePrefix("https://pokeapi.co/api/v2/pokemon/").removeSuffix("/").toInt()

    val avatarUrl: String
        get() = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/${id}.png"

    val likeIcon: ImageVector
        get() = if (like == LikeType.Like) Icons.Outlined.Favorite else Icons.Outlined.FavoriteBorder
}

enum class LikeType(type: Int){
    Like(0),
    Unlike(1)
}