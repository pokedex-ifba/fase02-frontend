package dev.cancio.finaldex.data.response

import dev.cancio.finaldex.data.model.Pokemon
import java.io.Serializable

data class PokemonPagination(
    val count: Int,
    val next: String?,
    val previous: String?,
    val results: List<Pokemon>
    ): Serializable{

}