package dev.cancio.finaldex.repository

import android.util.Log
import dev.cancio.finaldex.data.model.LikeType
import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.service.PokeService

class PokemonRepository {
    val api = PokeService.getInstance()
    val likedPokemon = mutableListOf<Pokemon>()

    suspend fun updatePokemon(pokemonId: String): LikeType  {
        return api.updatePokemon(pokemonId.toInt())
    }

    suspend fun getAllPokemon(): List<Pokemon> {
        val result = api.getPokemonList()
         return result.results
    }

    suspend fun getLikePokemon(): MutableList<Pokemon> {
        return likedPokemon
    }

    suspend fun getPokemonDetail(pokemonId: Int): Pokemon {
        return api.getPokemon(pokemonId)
    }
}