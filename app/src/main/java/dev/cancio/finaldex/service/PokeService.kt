package dev.cancio.finaldex.service

import dev.cancio.finaldex.data.model.LikeType
import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.data.response.PokemonPagination
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface PokeService {

    @GET("pokemon/")
    suspend fun getPokemonList(): PokemonPagination

    @GET("pokemon/{id}")
    suspend fun getPokemon(@Path("id") pokemonId: Int): Pokemon

    @GET("pokemonLike/")
    suspend fun getLikePokemon(): MutableList<Pokemon>

    @POST("pokemonLike/{id}")
    suspend fun updatePokemon(@Path("id") pokemonId: Int): LikeType

    companion object{
        private var pokeService: PokeService? = null
        fun getInstance() : PokeService {
            if (pokeService == null) {
                pokeService = Retrofit.Builder()
                    .baseUrl("https://pokeapi.co/api/v2/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(PokeService::class.java)
            }
            return pokeService!!
        }
    }
}