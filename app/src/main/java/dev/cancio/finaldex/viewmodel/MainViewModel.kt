package dev.cancio.finaldex.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import dev.cancio.finaldex.data.model.LikeType
import dev.cancio.finaldex.data.model.Pokemon
import dev.cancio.finaldex.repository.PokemonRepository
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private val repository = PokemonRepository()
    var pokemonList by mutableStateOf(listOf<Pokemon>())
    var pokemon by mutableStateOf(Pokemon())

    fun getPokemonOnHome(){
        viewModelScope.launch {
            pokemonList =repository.getAllPokemon()
        }
    }
    fun getPokemonOnDetail(): List<Pokemon>{
        viewModelScope.launch {
            pokemonList = repository.getLikePokemon()
        }
        return pokemonList
    }
    fun getPokemon(pokemonId: Int) {
        viewModelScope.launch {
            repository.getPokemonDetail(pokemonId)
        }
    }

    fun updatePokemon(pokemonId: String): LikeType{
        var likeType = LikeType.Unlike
        viewModelScope.launch {
            likeType = repository.updatePokemon(pokemonId)
        }
        return likeType
    }

    fun goToDetail(navController: NavController, id: Int) {
        navController.navigate("detail/${id}")
    }
}